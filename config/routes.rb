Rails.application.routes.draw do
  devise_for :users

  root 'posts#index'
  resources :posts , only: [:show, :index]
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'

    namespace :admin do
        resources :posts, except: [:show, :index]

    end
  end
end
