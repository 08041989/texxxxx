class Admin::PostsController < Admin::AdminController
before_action :authenticate_user!
before_action :set_post, only: [:show, :edit, :update, :destroy]


  def new
    @post = Post.new
  end
  def create
  #render plain: params[:post].inspect
   @post = Post.new(post_params)
  if (@post.save)
   redirect_to [:admin,@post], success: 'Статья успешно создана'
 else
   flash[:danger] = 'Статья не создана'
   render 'new'
 end
  end

  private def post_params
    params.require(:post).permit(:title, :body , :summary, :image)
  end
  def edit

end
def update

  if @post.update(post_params)
   redirect_to [:admin,@post], success: 'Статья успешно обновлена'
 else
   flash[:danger] = 'Статья не обновлена'

   render 'edit'
 end
end
def destroy
@post.destroy
redirect_to posts_path, success: 'Статья успешно удалена'
end
private
def set_post
@post = Post.find(params[:id])
end


end
